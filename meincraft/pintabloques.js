// registar un nuevo componente llamado 'pintabloques'

AFRAME.registerComponent('pintabloques', {

	// funcion que se llama al inicializar el componente
	init: function(){
		// lista de texturas de los bloques
		this.bloques = ['res/block00.jpg', 'res/block01.jpg', 'res/block02.jpg', 'res/block03.jpg'];
		this.bloque = 0; // bloque actual

		this.el.addEventListener('triggerdown', this.ponBloque.bind(this)); // gatillo para crear bloque
		this.el.addEventListener('gripdown', this.cambiaBloque.bind(this)); // botón lateral para cambiar de textura de bloque
	},

	cambiaBloque: function(){
		// usar textura siguiente
		this.bloque = (this.bloque + 1) % this.bloques.length;
	},

	ponBloque: function(){
		var mandopos = this.el.getAttribute('position'); // coger posición del mando
		var newpos = {}; // posición donde vamos a colocar el bloque
		newpos.x = Math.floor((mandopos.x) / 0.5) * 0.5;
		newpos.y = Math.floor((mandopos.y) / 0.5) * 0.5;
		newpos.z = Math.floor((mandopos.z) / 0.5) * 0.5;

		var cubo = document.createElement('a-entity');
		cubo.setAttribute('obj-model', {obj: 'resources/block.obj'});
		cubo.setAttribute('material', {src: this.bloques[this.bloque]});
		cubo.setAttribute('shadow', '');
		cubo.setAttribute('position', newpos);
		document.querySelector('a-scene').appendChild(cubo);
	}

});

